
var numberArr = [];

document.getElementById('btn__themSo').addEventListener("click", function themSo() {
    var valueN = document.getElementById('soN').value * 1;
    numberArr.push(valueN);
    // console.log(numberArr);
    var valueN = document.getElementById('soN').value = "";
    document.getElementById('ket-qua').innerText = numberArr;
});


// Bài 1
document.getElementById('btn__kqBai1').addEventListener('click', () => {
    var tong = 0;
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] >= 0) {
            tong += numberArr[index];
        }
    }
    document.getElementById('text__kqBai1').innerText = `Tổng: ${tong}`;
})

// <============================================================================================>

// Bài 2
document.getElementById('btn__kqBai2').addEventListener('click', () => {
    var soDuong = 0;
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] >= 0) {
            soDuong += 1;
        }
    }
    document.getElementById('text__kqBai2').innerText = `Số Dương: ${soDuong}`;

})
// <============================================================================================>

// Bài 3
document.getElementById('btn__kqBai3').addEventListener('click', () => {
    var min = numberArr[0];
    for (var index = 1; index < numberArr.length; index++) {
        if (min > numberArr[index]) {
            min = numberArr[index];
        }
    }
    document.getElementById('text__kqBai3').innerText = `Số nhỏ nhất: ${min}`;

})
// <============================================================================================>

// Bài 4
document.getElementById('btn__kqBai4').addEventListener('click', () => {
    var min;
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] >= 0) {
            min = numberArr[index];
            break;
        }
    }
    for (var index = 0; index < numberArr.length; index++) {


        if (numberArr[index] >= 0 && min > numberArr[index]) {
            min = numberArr[index];
        }
    }
    document.getElementById('text__kqBai4').innerText = `Số duong nhỏ nhất: ${min}`;

})
// <============================================================================================>

// Bài 5
document.getElementById('btn__kqBai5').addEventListener('click', () => {
    var DuongArr = [];
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] >= 0) {
            DuongArr.push(numberArr[index]);
        }
    }
    document.getElementById('text__kqBai5').innerText = `Số dương cuối cùng: ${DuongArr.pop()}`;
})
// <============================================================================================>

// Bài 6
document.getElementById('btn__kqBai6').addEventListener('click', () => {
    var viTri1 = document.getElementById('viTriDoi1').value * 1;
    var viTri2 = document.getElementById('viTriDoi2').value * 1;
    var temp = numberArr[viTri1];
    numberArr[viTri1] = numberArr[viTri2];
    numberArr[viTri2] = temp;
    document.getElementById('text__kqBai6').innerText = `Số dương cuối cùng: ${numberArr}`;

})
// <============================================================================================>

// Bài 7
document.getElementById('btn__kqBai7').addEventListener('click', () => {
    var temp = 0;
    for (var i = 0; i <= numberArr.length; i++) {
        for (var j = i + 1; j <= numberArr.length; j++) {
            if (numberArr[i] > numberArr[j]) {
                temp = numberArr[i];
                numberArr[i] = numberArr[j];
                numberArr[j] = temp;

            }
        }
    }
    document.getElementById('text__kqBai7').innerText = `sắp xếp: ${numberArr}`;

})
// <============================================================================================>

// Bài 8
function isPrime(n) {
    var flag = true;
    if (n < 2) {
        flag = false;
        return flag;
    }
    for (var i = 2; i < n; i++) {
        if (n % i == 0) {
            flag = false;
            break;
        }
    }
    return flag;
}
document.getElementById('btn__kqBai8').addEventListener('click', () => {

    var result = [];
    for (var index = 0; index < numberArr.length; index++) {
        if (isPrime(numberArr[index]) == true) {
            result.push(numberArr[index]);
        }
    }
    if (result.length == 0) {
        document.getElementById('text__kqBai8').innerText = `Số Nguyên tố đầu tiên: -1`;
    }
    document.getElementById('text__kqBai8').innerText = `Số Nguyên tố đầu tiên: ${result.shift()}`;
})
// <============================================================================================>

// Bài 9
document.getElementById('btn__kqBai9').addEventListener('click', () => {
    var soNguyen = 0;
    for (var index = 0; index < numberArr.length; index++) {
        if (Number.isInteger(numberArr[index]) == true) {
            soNguyen += 1;
        }
    }
    document.getElementById('text__kqBai9').innerText = `Số Nguyên: ${soNguyen}`;
})
// <============================================================================================>

// Bài 10
document.getElementById('btn__kqBai10').addEventListener('click', () => {

    var soDuong = 0;
    var soAm = 0
    for (var index = 0; index < numberArr.length; index++) {
        if (numberArr[index] >= 0) {
            soDuong += 1;
        }
        else {
            soAm += 1;
        }
    }
    if (soDuong == soAm) {
        document.getElementById('text__kqBai10').innerText = `Số Dương = Số Âm`;

    }
    else if (soDuong > soAm) {
        document.getElementById('text__kqBai10').innerText = `Số Dương > Số Âm`;
    }
    else {
        document.getElementById('text__kqBai10').innerText = `Số Dương < Số Âm`;
    }

})

// <============================================================================================>

const BT1 = document.getElementById('Bai1');
const BT2 = document.getElementById('Bai2');
const BT3 = document.getElementById('Bai3');
const BT4 = document.getElementById('Bai4');
const BT5 = document.getElementById('Bai5');
const BT6 = document.getElementById('Bai6');
const BT7 = document.getElementById('Bai7');
const BT8 = document.getElementById('Bai8');
const BT9 = document.getElementById('Bai9');
const BT10 = document.getElementById('Bai10');
const formBai1 = document.getElementById('form-bai1');
const formBai2 = document.getElementById('form-bai2');
const formBai3 = document.getElementById('form-bai3');
const formBai4 = document.getElementById('form-bai4');
const formBai5 = document.getElementById('form-bai5');
const formBai6 = document.getElementById('form-bai6');
const formBai7 = document.getElementById('form-bai7');
const formBai8 = document.getElementById('form-bai8');
const formBai9 = document.getElementById('form-bai9');
const formBai10 = document.getElementById('form-bai10');

BT1.addEventListener('click', () => {
    formBai1.classList.add('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT2.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.add('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT3.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.add('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT4.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.add('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT5.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.add('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT6.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.add('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT7.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.add('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT8.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.add('active');
    formBai9.classList.remove('active');
    formBai10.classList.remove('active');
})
BT9.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.add('active');
    formBai10.classList.remove('active');
})
BT10.addEventListener('click', () => {
    formBai1.classList.remove('active');
    formBai2.classList.remove('active');
    formBai3.classList.remove('active');
    formBai4.classList.remove('active');
    formBai5.classList.remove('active');
    formBai6.classList.remove('active');
    formBai7.classList.remove('active');
    formBai8.classList.remove('active');
    formBai9.classList.remove('active');
    formBai10.classList.add('active');
})